import React, {lazy, useState, Suspense, useEffect} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import {useLocation} from "react-router-dom";
import TopMenu from "./components/layout/top-menu/TopMenu";
import Navbar from "./components/layout/navbar/Navbar";
import WaitingList from "./components/layout/waiting-list/WaitingList";
import Loader from "./components/layout/loader/Loader";
const Home = lazy(() => import('./components/home/Home'));
const Store = lazy(() => import('./components/store/Store'));
const Broadcast = lazy(() => import('./components/broadcast/Broadcast'));
const Courses = lazy(() => import('./components/courses/Courses'));
const CourseDetails = lazy(() => import('./components/courses/course-details/CourseDetails'));


const App = () => {
    const [ShowWaiting, setShowWaiting] = useState(false);

    const ScrollToTop = () => {
        const {pathname} = useLocation();

        useEffect(() => {
            window.scrollTo(0, 0);
        }, [pathname]);

        return null;
    }


    const waitingToggler = (toggleWaiting) => {
        setShowWaiting(toggleWaiting);
    }
    return (
        <Router>
            <WaitingList ShowWaiting={waitingToggler} isShowed={ShowWaiting}/>
            <div className={`site-full-container ${ShowWaiting ? 'slide' : ''}`}>
                <TopMenu ShowWaiting={waitingToggler} isShowed={ShowWaiting}/>
                <div className="layout-container">
                    <Navbar/>
                    <Suspense fallback={<Loader />}>
                        <ScrollToTop/>
                        <div className="main-container courses-container-fluid">
                            <Switch>
                                <Route path="/" exact component={Home}/>
                                <Route path="/store/:id" component={Store}/>
                                <Route path="/broadcast/" component={Broadcast}/>
                                <Route path="/courses/" exact component={Courses}/>
                                <Route path="/courses/:id" component={CourseDetails}/>
                            </Switch>
                        </div>
                    </Suspense>
                </div>
            </div>
        </Router>
    );
}


export default App;
