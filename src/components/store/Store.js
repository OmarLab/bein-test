import React, {useEffect, useState} from 'react';
import {VideoPlayer} from "../layout/player/VideoPlayer";
import './Store.css'
import DocPlayer from "../layout/doc-player/DocPlayer";
import StoreCard from "../layout/store-cards/StoreCard";
import demoData from '../layout/store-cards/StoreData'
const Store = (match) => {
    const [ProductId, setProductId] = useState('');

    useEffect(() => {
        setProductId(match.match.params.id);
    },[match.match.params.id])
    return (
        <div>
            <div className="row">
                <div className="home-video-col">
                    {ProductId === '1' ? <VideoPlayer poster={'/images/second-video-bg.png'}/> : <DocPlayer />}
                </div>
                <div className="home-details-col">
                    <div className="row other-videos-row">
                        {demoData.map((item, number) => (
                            number < 7 ? <div className="col-6 col-sm-4" key={number}>
                            <StoreCard item={item} />
                            </div> : null
                        ))}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Store