import React from 'react';
import './DocPlayer.css'


const DocPlayer = () => {

    return (
        <div className="doc-player">
            <div className="download-section">
                <div className="download-section-img-container">
                    <img src="/images/shopping-cart.svg" className="download-section-img" alt=""/>
                </div>
                <span className="download-section-text">تحميل / شراء</span>
            </div>
            <div className="row doc-player-bottom">
                <div className="col-4 col-sm-6 d-flex justify-content-start align-items-end">
                    <div>
                        <img className="download-icon" src="/images/download-icon.svg" alt=""/>
                        <span className="download-text">120</span>
                    </div>
                </div>
                <div className="col-8 col-sm-6">
                    <div className="d-flex align-items-center justify-content-end">
                        <span className="type-title">PDF</span>
                        <img className="type-icon" src="/images/pdf-icon.svg"
                             alt=""/>
                    </div>
                    <h5>
                        دورة صناعة المحتوى مع هند الناهض
                    </h5>
                    <div className="doc-player-price-section" dir="auto">
                        <span className="store-card-price">300</span>
                        <span className="store-card-currency">دينار كويتي</span>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default DocPlayer