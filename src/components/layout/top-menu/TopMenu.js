import React, {useState} from "react";
import './TopMenu.css'
import {faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const TopMenu = (props) => {
    const [ShowMobileNav, setShowMobileNav] = useState(false);

    return (
        <div className="main-menu-container">
            <div className={`mobile-navbar ${ShowMobileNav ? 'show' : ''}`}>
                <div className="close-mobile-navbar" onClick={() => setShowMobileNav(false)}>
                    <FontAwesomeIcon icon={faTimes}/>
                </div>
                <div className="mobile-navbar-menu">
                    <a>
                        <div className="mobile-nav-link-text home-link">الرئيسية</div>
                    </a>
                    <a>
                        <div className="mobile-nav-link-text">نبذه عنا</div>
                    </a>
                    <a>
                        <div className="mobile-nav-link-text">الخبراء</div>
                    </a>
                    <a>
                        <div className="mobile-nav-link-text">سؤال وجواب</div>
                    </a>
                    <a>
                        <div className="mobile-nav-link-text">اختبارات كورونا</div>
                    </a>
                    <a>
                        <div className="mobile-nav-link-text">أتصل بنا</div>
                    </a>
                    <a>
                        <div className="mobile-nav-link-text expert-join">أنضم كخبير</div>
                    </a>
                </div>
            </div>
                <div className="courses-top-menu courses-container-fluid">
                    <img className="d-block d-lg-none" src="/images/mobile-menu-icon.svg" onClick={() => setShowMobileNav(!ShowMobileNav)} alt=""/>
                    <div className="right-section" onClick={() => {props.ShowWaiting(!props.isShowed)}}>
                        <h2 className="waiting-list-title" dir="auto">
                            ١٤ قائمة الانتظار
                        </h2>
                        <div className="back-button">
                            <img src="/images/arrow-right.svg" alt=""/>
                        </div>
                    </div>
                    <div className="left-section">
                        <span className="menu-text">مرحبا بك <span className="menu-name">مالك محمد</span></span>
                        <div className="user-icon-container">
                            <img src="/images/user-icon.svg" alt=""/>
                        </div>
                        <img className="notification-icon" src="/images/notification-icon.svg" alt=""/>
                    </div>
                </div>
        </div>
    )
}


export default TopMenu