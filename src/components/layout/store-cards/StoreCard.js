import React from 'react';
import {Link} from "react-router-dom";
import {Card} from "react-bootstrap";
import './StoreCard.css'

const StoreCard = (props) => {
    return (
        <Card className="store-card">
            <Link to={`/store/${props.item.typeId}`}>
                <div className="store-card-img-container">
                    {props.item.img ? <Card.Img variant="top" src={props.item.img}/> : <Card.Img variant="top" src="/images/card-placeholder.jpg"/>}
                    <div className="type-section">
                        {!props.item.isVideo ? <span className="type-text">{props.item.type}</span> : null}
                        <img className="type-icon" src={`/images/${props.item.type}-icon.svg`}
                             alt=""/>
                    </div>
                </div>
            </Link>
            <Card.Body>
                <Link to={`/store/${props.item.id}`}>
                    <Card.Title className="text-white">{props.item.title}</Card.Title>
                </Link>
                <div className="row no-gutters">
                    <div className="col-4 text-left">
                        <img className="download-icon" src="/images/download-icon.svg" alt=""/>
                        <span className="download-text">{props.item.downloads}</span>
                    </div>
                    <div className="col-8" dir="auto">
                        <span className="store-card-price">{props.item.price}</span>
                        <span className="store-card-currency">دينار كويتي</span>
                    </div>
                </div>
            </Card.Body>
        </Card>
    )
}

export default StoreCard