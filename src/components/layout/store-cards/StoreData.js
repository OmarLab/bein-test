const StoreData = [
  {
    "typeId": 1,
    "category": 1,
    "img": '/images/card-img1.jpg',
    "title": 'دورة صناعة المحتوى مع هند الناهض',
    "price": '233',
    "isVideo": true,
    "VideoId": 1,
    "type": 'video',
    "downloads": '12'
  },
  {
    "typeId": 1,
    "category": 1,
    "img": '/images/card-img2.jpg',
    "title": 'تعرف على أسرار صناعة المحتوى التسويقي الفعال',
    "price": '123',
    "isVideo": true,
    "VideoId": 2,
    "type": 'video',
    "downloads": '654'
  },
  {
    "typeId": 1,
    "category": 2,
    "img": '/images/card-img3.jpg',
    "title": 'تعرف على أسرار صناعة المحتوى التسويقي الفعال',
    "price": '255',
    "isVideo": true,
    "VideoId": 3,
    "type": 'video',
    "downloads": '345'
  },
  {
    "typeId": 2,
    "category": 1,
    "img": '',
    "title": 'تعرف على أسرار صناعة المحتوى التسويقي الفعال',
    "price": '455',
    "isVideo": false,
    "type": 'AUDIO',
    "downloads": '123'
  },
  {
    "typeId": 2,
    "category": 3,
    "img": '',
    "title": 'فوكس حلقة ٥',
    "price": '234',
    "isVideo": false,
    "type": 'WORD',
    "downloads": '34'
  },
  {
    "typeId": 2,
    "category": 1,
    "img": '',
    "title": 'دورة صناعة المحتوى مع هند الناهض',
    "price": '432',
    "isVideo": false,
    "type": 'PDF',
    "downloads": '122'
  },
  {
    "typeId": 1,
    "category": 1,
    "img": '/images/card-img4.jpg',
    "title": 'دورة صناعة المحتوى مع هند الناهض',
    "price": '233',
    "isVideo": true,
    "VideoId": 4,
    "type": 'video',
    "downloads": '12'
  },
  {
    "typeId": 1,
    "category": 1,
    "img": '/images/card-img2.jpg',
    "title": 'تعرف على أسرار صناعة المحتوى التسويقي الفعال',
    "price": '123',
    "isVideo": true,
    "VideoId": 5,
    "type": 'video',
    "downloads": '654'
  },
  {
    "typeId": 1,
    "category": 1,
    "img": '/images/card-img2.jpg',
    "title": 'تعرف على أسرار صناعة المحتوى التسويقي الفعال',
    "price": '123',
    "isVideo": true,
    "VideoId": 6,
    "type": 'video',
    "downloads": '654'
  },
  {
    "typeId": 1,
    "category": 3,
    "img": '/images/card-img1.jpg',
    "title": 'دورة صناعة المحتوى مع هند الناهض',
    "price": '233',
    "isVideo": true,
    "VideoId": 7,
    "type": 'video',
    "downloads": '12'
  },
  {
    "typeId": 1,
    "category": 1,
    "img": '/images/card-img4.jpg',
    "title": 'تعرف على أسرار صناعة المحتوى التسويقي الفعال',
    "price": '123',
    "isVideo": true,
    "VideoId": 8,
    "type": 'video',
    "downloads": '654'
  },
  {
    "typeId": 1,
    "category": 2,
    "img": '/images/card-img2.jpg',
    "title": 'تعرف على أسرار صناعة المحتوى التسويقي الفعال',
    "price": '123',
    "isVideo": true,
    "VideoId": 9,
    "type": 'video',
    "downloads": '654'
  },
  {
    "typeId": 1,
    "category": 3,
    "img": '/images/card-img1.jpg',
    "title": 'دورة صناعة المحتوى مع هند الناهض',
    "price": '233',
    "isVideo": true,
    "VideoId": 10,
    "type": 'video',
    "downloads": '12'
  },
  {
    "typeId": 1,
    "category": 3,
    "img": '/images/card-img4.jpg',
    "title": 'تعرف على أسرار صناعة المحتوى التسويقي الفعال',
    "price": '123',
    "isVideo": true,
    "VideoId": 11,
    "type": 'video',
    "downloads": '654'
  },
  {
    "typeId": 1,
    "category": 2,
    "img": '/images/card-img1.jpg',
    "title": 'دورة صناعة المحتوى مع هند الناهض',
    "price": '233',
    "isVideo": true,
    "VideoId": 12,
    "type": 'video',
    "downloads": '12'
  }
]

export default StoreData