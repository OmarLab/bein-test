import React from 'react';
import './WaitingList.css'
import WaitingData from "./WaitingData";
import {faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const WaitingList = (props) => {
    return (
        <div className="waiting-list-menu">
            <div className="waiting-list-menu-title" onClick={() => {props.ShowWaiting(!props.isShowed)}}>
                <FontAwesomeIcon className="d-block d-lg-none" icon={faTimes}/>
                <h2>١٤ قائمة الانتظار</h2>
            </div>
            <div className="waiting-list-items">
                {WaitingData.map((item, number) => (
                    <div className="waiting-list-item" key={item.userId}>
                        <div className="waiting-list-item-text">
                            <span className="since-when">
                                {`منذ ${item.minutes} دقيقة`}
                            </span>
                            <h5 className="waiting-list-title">
                                {item.username}
                            </h5>
                        </div>
                        <div className="waiting-list-img-container">
                            <img src="/images/waiting-user-icon.svg" alt=""/>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default WaitingList