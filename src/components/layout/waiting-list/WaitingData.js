const WaitingData = [
    {
        "userId": 1,
        "username": 'مستخدم 1',
        "minutes": '43'
    },
    {
        "userId": 2,
        "username": 'مستخدم 2',
        "minutes": '12'
    },
    {
        "userId": 3,
        "username": 'مستخدم 3',
        "minutes": '42'
    },
    {
        "userId": 4,
        "username": 'مستخدم 4',
        "minutes": '15'
    },
    {
        "userId": 5,
        "username": 'مستخدم 5',
        "minutes": '26'
    },
    {
        "userId": 6,
        "username": 'مستخدم 6',
        "minutes": '54'
    },
    {
        "userId": 7,
        "username": 'مستخدم 7',
        "minutes": '19'
    },
]

export default WaitingData