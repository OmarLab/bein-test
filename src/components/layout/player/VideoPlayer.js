import React, {useRef, useEffect, useState} from "react";
import videojs from "video.js";
import 'video.js/dist/video-js.css';
import './VideoPlayer.css'

export const VideoPlayer = props => {

    console.log(props);
    const videoPlayerRef = useRef(null);
    const videoSrc = "/videos/test.mp4";
    const videoJSOptions = {
        controls: true,
        userActions: {hotkeys: true},
        poster: props.poster
    };

    useEffect(() => {
        if (videoPlayerRef) {
            const player = videojs(videoPlayerRef.current, videoJSOptions, () => {
                player.src(videoSrc);
            });
            player.on("play", () => {
                let node = document.getElementsByClassName('video-caption-title')
                node[0].classList.remove('show');
            })
        }

        return () => {
        };
    }, [videoJSOptions]);

    return (
        <div className="home-video">
            <h5 className="video-caption-title show">
                {props.captionTitle ? props.captionTitle : 'المقدمة التعريفية لـ هند الناهض'}
            </h5>
            <video
                ref={videoPlayerRef}
                className="video-js"
            />
        </div>
    );
};