import React, {useEffect, useState} from 'react';
import {Nav} from "react-bootstrap";
import './Navbar.css'
import {Link, useLocation} from "react-router-dom";

const Navbar = () => {
    const location = useLocation();
    const [CurrentPath, setCurrentPath] = useState('');
    const [ClearCurrentPath, setClearCurrentPath] = useState('');

    useEffect(() => {
        const currentPath = location.pathname;
        setCurrentPath(currentPath)
        const clearCurrentPath = currentPath.substring(0, currentPath.lastIndexOf("/") + 1);
        setClearCurrentPath(clearCurrentPath)
    }, [location]);
    return (
        <div className="second-bar-container">
            <div className="courses-second-menu-container">
                <div className="courses-second-menu courses-container-fluid">
                    <div className="row">
                        <div className="col-12 col-lg-5 d-flex justify-content-between mobile-notification-column">
                            <div className="right-section">
                                <img src="/images/logo.svg" alt=""/>
                                <div className="top-logo-text">
                                    <h5 className="top-logo-text-en text-white">timeviewer</h5>
                                    <h5 className="top-logo-text-ar text-white">العيادة الرقمية</h5>
                                </div>
                            </div>
                            <div className="mobile-notification d-flex align-items-center d-lg-none">
                                <div className="user-icon-container">
                                    <img src="/images/user-icon.svg" alt=""/>
                                </div>
                                <img src="/images/white-notification-icon.svg" alt=""/>
                            </div>
                        </div>
                        <div className="col-12 col-lg-7 d-none d-lg-block">
                            <div className="left-section">
                                <button className="btn">
                                    مشغول باستشارة
                                </button>
                                <div className="user-top-details">
                                    <h5>العيادة الرقمية
                                    </h5>
                                    <p className="semi-title">
                                        لـ د. هند الناهض
                                    </p>
                                    <div>
                                        <p className="top-rate-text">تقييم</p>
                                        <img src="/images/grey-star.svg" alt=""/>
                                        <img src="/images/star.svg" alt=""/>
                                        <img src="/images/star.svg" alt=""/>
                                        <img src="/images/star.svg" alt=""/>
                                        <img src="/images/star.svg" alt=""/>
                                    </div>
                                </div>
                                <img className="top-profile-img" src="/images/profile-img.png" alt=""/>
                                <img className="top-home-icon" src="/images/home-icon.svg" alt=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="navbar-container">
                <div className="main-menu-row">
                    <div className="main-menu-column d-flex navbar-items-col">
                        <div className="w-100 d-flex menu-icons-container">
                            <div className="nav-column">
                                <div className="navbar-item">
                                    <div className="navbar-item-text">
                                        <div>
                                            <h5>سرعة الأنترنت</h5>
                                            <p>٣٦ Mbps</p>
                                        </div>
                                    </div>
                                    <div className="wifi-icon-container">
                                        <img src="/images/wifi-icon.svg" alt=""/>
                                    </div>
                                </div>
                            </div>
                            <div className="nav-column">
                                <div className="navbar-item">
                                    <div className="navbar-item-text">
                                        <div className="d-none d-lg-block">
                                            <h5>تجربة الكاميرا</h5>
                                            <p>لم تقم باختبارها</p>
                                        </div>
                                    </div>
                                    <div className="camera-icon-container">
                                        <img src="/images/camera-icon.svg" alt=""/>
                                    </div>
                                </div>
                            </div>
                            <div className="nav-column">
                                <div className="navbar-item">
                                    <div className="navbar-item-text">
                                        <div className="d-none d-lg-block">
                                            <h5>تجربة الصوت</h5>
                                            <p>جيد جدا</p>
                                        </div>
                                    </div>
                                    <div className="mic-icon-container">
                                        <img src="/images/mic-icon.svg" alt=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-lg-7 mobile-menu-details-column d-block d-lg-none">
                        <div className="left-section">
                            <button className="btn">
                                مشغول باستشارة
                            </button>
                            <div className="user-top-details">
                                <h5>العيادة الرقمية
                                </h5>
                                <p className="semi-title">
                                    لـ د. هند الناهض
                                </p>
                                <div>
                                    <p className="top-rate-text">تقييم</p>
                                    <img src="/images/grey-star.svg" alt=""/>
                                    <img src="/images/star.svg" alt=""/>
                                    <img src="/images/star.svg" alt=""/>
                                    <img src="/images/star.svg" alt=""/>
                                    <img src="/images/star.svg" alt=""/>
                                </div>
                            </div>
                            <img className="top-profile-img" src="/images/profile-img.png" alt=""/>
                        </div>
                    </div>
                    <div className="main-menu-column d-flex align-items-center navbar-links-col">
                        <Nav activeKey="/">
                            <div className="mobile-overlay">
                            </div>
                            <Nav.Item className="d-none d-sm-block">
                                <a className="nav-link">حجز عيادة (20 دينار كويتي)</a>
                            </Nav.Item>
                            <Nav.Item className={`d-none d-sm-flex ${ClearCurrentPath === '/courses/' || CurrentPath === '/courses' ? 'active' : 'null'}`}>
                                <Nav.Link  as={Link} to="/courses" eventKey="2">
                                    <span>كورسات</span>
                                    {ClearCurrentPath === '/courses/' ? <img className="menu-arrow" src="/images/back-arrow.svg" alt=""/> : null}
                                </Nav.Link>
                            </Nav.Item>
                            <Nav.Item className={`d-none d-sm-flex ${CurrentPath === '/broadcast' ? 'active' : 'null'}`}>
                                <Nav.Link  as={Link} to="/broadcast" eventKey="3">برودكاست</Nav.Link>
                            </Nav.Item>
                            <Nav.Item className={`d-none d-sm-flex ${ClearCurrentPath === '/store/' ? 'active' : 'null'}`}>
                                <Nav.Link  as={Link} to="/store/1" eventKey="4">متجر</Nav.Link>
                            </Nav.Item>
                            <Nav.Item className={`d-none d-sm-flex ${CurrentPath === '/' ? 'active' : 'null'}`}>
                                <Nav.Link  as={Link} to="/" eventKey="5">نبذه عن هند</Nav.Link>
                            </Nav.Item>
                            <Nav.Item className={`d-flex d-sm-none ${CurrentPath === '/' ? 'active' : 'null'}`}>
                                <Nav.Link  as={Link} to="/" eventKey="5">نبذه عن هند</Nav.Link>
                            </Nav.Item>
                            <Nav.Item className={`d-flex d-sm-none ${ClearCurrentPath === '/store/' ? 'active' : 'null'}`}>
                                <Nav.Link  as={Link} to="/store/1" eventKey="4">متجر</Nav.Link>
                            </Nav.Item>
                            <Nav.Item className={`d-flex d-sm-none ${CurrentPath === '/broadcast' ? 'active' : 'null'}`}>
                                <Nav.Link  as={Link} to="/broadcast" eventKey="3">برودكاست</Nav.Link>
                            </Nav.Item>
                            <Nav.Item className={`d-flex d-sm-none ${ClearCurrentPath === '/courses/' || CurrentPath === '/courses' ? 'active' : 'null'}`}>
                                <Nav.Link className="courses-mobile-nav" as={Link} to="/courses" eventKey="2">
                                    <span>كورسات</span>
                                    {ClearCurrentPath === '/courses/' ? <img className="menu-arrow" src="/images/back-arrow.svg" alt=""/> : null}
                                </Nav.Link>
                            </Nav.Item>
                            <Nav.Item className="d-flex d-sm-none">
                                <a className="nav-link">حجز عيادة (20 دينار كويتي)</a>
                            </Nav.Item>
                        </Nav>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Navbar