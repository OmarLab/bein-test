const CoursesData = [
    {
        "typeId": 1,
        "img": '/images/wide-card-img1.jpg',
        "title": 'تعرف على أسرار صناعة المحتوى التسويقي الفعال',
        "price": '255',
        "videos_number": '12'
    },
    {
        "typeId": 2,
        "img": '/images/wide-card-img2.jpg',
        "title": 'دورة صناعة المحتوى مع هند الناهض',
        "price": '233',
        "videos_number": '12'
    },
    {
        "typeId": 3,
        "img": '/images/wide-card-img3.jpg',
        "title": 'تعرف على أسرار صناعة المحتوى التسويقي الفعال',
        "price": '123',
        "videos_number": '16'
    },
    {
        "typeId": 4,
        "img": '/images/wide-card-img4.jpg',
        "title": 'تعرف على أسرار صناعة المحتوى التسويقي الفعال',
        "price": '123',
        "videos_number": '23'
    },
    {
        "typeId": 5,
        "img": '/images/wide-card-img5.jpg',
        "title": 'تعرف على أسرار صناعة المحتوى التسويقي الفعال',
        "price": '233',
        "videos_number": '5'
    },
    {
        "typeId": 6,
        "img": '/images/wide-card-img3.jpg',
        "title": 'دورة صناعة المحتوى مع هند الناهض',
        "price": '123',
        "videos_number": '6'
    },
    {
        "typeId": 7,
        "img": '/images/wide-card-img1.jpg',
        "title": 'تعرف على أسرار صناعة المحتوى التسويقي الفعال',
        "price": '123',
        "videos_number": '26'
    },
    {
        "typeId": 8,
        "img": '/images/wide-card-img5.jpg',
        "title": 'دورة صناعة المحتوى مع هند الناهض',
        "price": '233',
        "videos_number": '7'
    },
    {
        "typeId": 9,
        "img": '/images/wide-card-img4.jpg',
        "title": 'تعرف على أسرار صناعة المحتوى التسويقي الفعال',
        "price": '123',
        "videos_number": '31'
    },
    {
        "typeId": 10,
        "img": '/images/wide-card-img2.jpg',
        "title": 'تعرف على أسرار صناعة المحتوى التسويقي الفعال',
        "price": '233',
        "videos_number": '19'
    }
]

export default CoursesData