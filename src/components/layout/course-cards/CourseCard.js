import React from 'react';
import {Link} from "react-router-dom";
import {Card} from "react-bootstrap";
import './CourseCard.css'

const CourseCard = (props) => {
    return (
        <Card className="course-card">
            <Link to={`/courses/${props.item.typeId}`}>
                <div className="store-card-img-container">
                    {props.item.img ? <Card.Img variant="top" src={props.item.img}/> : null}
                    <div className="type-section">
                        <img className="type-icon" src="/images/orange-video-icon.svg"
                             alt=""/>
                        <span className="type-text" dir="auto">{props.item.videos_number} فيديو</span>

                    </div>
                </div>
            </Link>
            <Card.Body>
                <div className="row no-gutters">
                    <div className="course-card-price-column d-flex align-items-end justify-content-end text-left" dir="auto">
                        <span className="store-card-price">{props.item.price}</span>
                        <span className="store-card-currency">دينار كويتي</span>
                    </div>
                    <div className="course-card-title-column">
                        <Link to={`/courses/${props.item.typeId}`}>
                            <Card.Title className="text-white">{props.item.title}</Card.Title>
                        </Link>
                    </div>
                </div>
            </Card.Body>
        </Card>
    )
}

export default CourseCard