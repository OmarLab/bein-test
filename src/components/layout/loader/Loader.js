import React from 'react';
import './Loader.css'

const Loader = () => {
    return (
        <div className="loader-container">
            <div className="loader-icon">
            </div>
        </div>
    );
}

export default Loader