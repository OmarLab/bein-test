import React from 'react';
import {VideoPlayer} from "../layout/player/VideoPlayer";
import demoData from "../layout/store-cards/StoreData";
import StoreCard from "../layout/store-cards/StoreCard";

const Broadcast = () => {
    return (
        <div className="fadein-container">
            <div className="row">
                <div className="home-video-col">
                    <VideoPlayer poster={'/images/second-video-bg.png'}/>
                </div>
                <div className="home-details-col">
                    <div className="row other-videos-row">
                        {demoData.map((item, number) => (
                            item.isVideo ? <div className="col-6 col-sm-4" key={number}>
                                <StoreCard item={item} />
                            </div> : null
                        ))}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Broadcast