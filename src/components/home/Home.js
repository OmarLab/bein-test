import React from 'react';
import './home.css'
import { VideoPlayer } from "../layout/player/VideoPlayer";


const Home = () => {

    return (
        <div className="fadein-container">
            <div className="row">
                <div className="home-video-col">
                    <VideoPlayer poster={'/images/main-video-bg.jpg'}/>
                </div>
                <div className="home-details-col">
                    <div className="home-sided-text-container">
                        <p className="mb-0">
                            هند الناهض مستشارة إعلام رقمي, مستشارة الإعلام الإلكتروني لمكتب وكيل وزارة الإعلام الكويتية
                            وهي المؤسسة لشركة “سوشالوبي” للخدمات المختصة في مجال الإعلام الإجتماعي , بالإضافة الى إنها
                            مستشارة في مجال التواصل الاجتماعي.هند الناهض مستشارة إعلام رقمي, مستشارة الإعلام الإلكتروني
                            لمكتب وكيل وزارة الإعلام الكويتية وهي المؤسسة لشركة “سوشالوبي” للخدمات المختصة في مجال
                            الإعلام الإجتماعي , بالإضافة الى إنها مستشارة في مجال التواصل الاجتماعي.
                        </p>
                    </div>
                    <h5 className="home-experiences-title">
                        الخبرات
                    </h5>
                    <div className="home-experiences-tags">
                        <div className="d-inline-block home-tag">
                            تطوير الأعمال
                        </div>
                        <div className="d-inline-block home-tag">
                            المشاريع الصغيرة
                        </div>
                        <div className="d-inline-block home-tag">
                            مشاركة العملاء
                        </div>
                        <div className="d-inline-block home-tag">
                            الإعلام
                        </div>
                        <div className="d-inline-block home-tag">
                            التحدث أمام الجمهور
                        </div>
                        <div className="d-inline-block home-tag">
                            الدعاية والإعلان
                        </div>
                        <div className="d-inline-block home-tag">
                            ريادة الأعمال
                        </div>
                        <div className="d-inline-block home-tag">
                            القيادة
                        </div>
                        <div className="d-inline-block home-tag">
                            التسويق الرقمي
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default Home