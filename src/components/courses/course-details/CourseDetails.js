import React, {useEffect, useState} from 'react';
import {VideoPlayer} from "../../layout/player/VideoPlayer";
import './CourseDetails.css'
import CoursesData from "../../layout/course-cards/CoursesData";
import demoData from "../../layout/store-cards/StoreData";
import StoreCard from "../../layout/store-cards/StoreCard";

const CourseDetails = (match) => {
    const [CourseId, setCourseId] = useState('');

    useEffect(() => {
        setCourseId(match.match.params.id);
    }, [match.match.params.id])
    return (
        <div className="fadein-container">
            <div className="row">
                <div className="home-video-col d-none d-xl-block">
                    {CoursesData.map((item,number) => (
                        item.typeId == CourseId ? <VideoPlayer key={number} poster={item.img} captionTitle={item.title}/>
                            : null
                    ))}
                </div>
                <div className="home-details-col">
                    <div className="home-sided-text-container">
                        <h2 className="course-description-title">وصف الكورس</h2>
                        <p className="course-description-text mb-0">
                            لوريم إيبسوم هو ببساطة نص شكلي بمعنى أن الغاية هي الشكل وليس المحتوى ويُستخدم في صناعات
                            المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما
                            قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو
                            مرجع شكلي لهذه الأحرف.
                        </p>
                    </div>
                    <div className="home-video-col p-0 d-block d-xl-none">
                        {CoursesData.map((item,number) => (
                            item.typeId == CourseId ? <VideoPlayer key={number} poster={item.img}  />
                                : null
                        ))}
                    </div>
                    <div className="course-details-side-container row other-videos-row">
                        {demoData.map((item, number) => (
                            item.isVideo && item.category === 1 ? <div className="col-6 col-sm-4" key={number}>
                                <StoreCard item={item} />
                            </div> : null
                        ))}
                    </div>
                    <h5 className="course-details-side-title">
                        صناعة محتوي تسويقي فعال
                    </h5>
                    <div className="course-details-side-container row other-videos-row">
                        {demoData.map((item, number) => (
                            item.isVideo && item.category === 2 ? <div className="col-6 col-sm-4" key={number}>
                                <StoreCard item={item} />
                            </div> : null
                        ))}
                    </div>
                    <h5 className="course-details-side-title">
                        صناعة محتوي تسويقي فعال
                    </h5>
                    <div className="course-details-side-container row other-videos-row">
                        {demoData.map((item, number) => (
                            item.isVideo && item.category === 2 ? <div className="col-6 col-sm-4" key={number}>
                                <StoreCard item={item} />
                            </div> : null
                        ))}
                    </div>
                </div>
            </div>
        </div>

    );
}

export default CourseDetails