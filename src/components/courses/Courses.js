import React from 'react';
import CoursesData from "../layout/course-cards/CoursesData";
import CourseCard from "../layout/course-cards/CourseCard";

const Courses = () => {
    return (
        <div className="fadein-container">
            <div className="row courses-container">
                {CoursesData.map((item, number) => (
                    <div className="col-12 col-sm-6 col-lg-4 col-xl-3" key={number}>
                        <CourseCard item={item}/>
                    </div>
                ))}

            </div>
        </div>
    );
}

export default Courses